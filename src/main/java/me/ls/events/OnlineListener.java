package me.ls.events;

import me.ls.sql.MySql;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.HashMap;

public class OnlineListener implements Listener {

    private final HashMap<String, Long> loginTimes = new HashMap<>();

    @EventHandler
    public void onConnect(LoginEvent e){

        //Save login-time of player
        loginTimes.put(e.getConnection().getUniqueId().toString(), System.currentTimeMillis());

    }

    @EventHandler
    public void onDisconnect(PlayerDisconnectEvent e){
        //Get last time player logged in and calculate time they were online
        long loginTime = loginTimes.get(e.getPlayer().getUniqueId().toString());
        long onlineSince = System.currentTimeMillis()-loginTime;

        //Update MySQL
        MySql.getInstance().addPlayTime(e.getPlayer().getUniqueId().toString(), onlineSince);

        //Clear from memory
        loginTimes.remove(e.getPlayer().getUniqueId().toString());
    }

}
