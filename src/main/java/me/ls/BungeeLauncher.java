package me.ls;

import me.ls.events.OnlineListener;
import me.ls.sql.MySql;
import net.md_5.bungee.api.plugin.Plugin;

import java.sql.SQLException;

public class BungeeLauncher extends Plugin {

    private static Plugin plugin;

    @Override
    public void onEnable() {
        plugin = this;
        new MySql();

        getProxy().getPluginManager().registerListener(this, new OnlineListener());
    }

    @Override
    public void onDisable() {
        try {
            MySql.getInstance().getConnection().close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static Plugin getPlugin() {
        return plugin;
    }
}
