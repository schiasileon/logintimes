package me.ls.sql;

import me.ls.BungeeLauncher;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;

public class SqlConfigProvider {

    private final String host;
    private final int port;
    private final String db;
    private final String passwd;
    private final String user;

    private final String file;

    private final static ConfigurationProvider provider = ConfigurationProvider.getProvider(YamlConfiguration.class);

    public SqlConfigProvider(String file) throws IOException {
        this.file = file;

        Configuration configuration = prepareConfig();

        host = configuration.getString("host");
        port = configuration.getInt("port");
        db = configuration.getString("db_name");
        passwd = configuration.getString("passwd");
        user = configuration.getString("user");
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public String getDb() {
        return db;
    }

    public String getPasswd() {
        return passwd;
    }

    public String getUser() {
        return user;
    }

    private Configuration prepareConfig() throws IOException {
        if(!BungeeLauncher.getPlugin().getDataFolder().exists()){
            BungeeLauncher.getPlugin().getDataFolder().mkdirs();
        }

        Path confFile = Path.of(BungeeLauncher.getPlugin().getDataFolder().toPath()+"/"+file);
        if(!Files.exists(confFile)){
            Path defaultFile;
            try {
                URI uri =getClass().getClassLoader().getResource("config.yml").toURI();
                FileSystems.newFileSystem(uri, Collections.emptyMap());
                defaultFile = Paths.get(uri);
            } catch (URISyntaxException e) {
                throw new IOException(e.getMessage());
            }
            Files.copy(defaultFile, confFile);
        }
        return provider.load(confFile.toFile()).getSection("mysql");
    }


}
