package me.ls.sql;

import java.io.IOException;
import java.sql.*;

public class MySql {

    private final Connection connection;
    private static MySql instance;

    public MySql() {
        instance = this;
        SqlConfigProvider provider;
        try {
            provider = new SqlConfigProvider("config.yml");
        } catch (IOException ioException) {
            throw new IllegalArgumentException("Config file error: " + ioException.getMessage());
        }

        try {
            connection = DriverManager.getConnection("jdbc:mysql://" + provider.getHost()+":"+provider.getPort() + "/"
                            + provider.getDb(),
                    provider.getUser(), provider.getPasswd());
        } catch (SQLException throwables) {
            throw new IllegalStateException("Unable to connect to MySQL DB");
        }

        init();

    }

    private void init(){
        try (PreparedStatement tableCreate = connection.prepareStatement("""
                CREATE TABLE IF NOT EXISTS logins (
                uuid varchar(50) primary key,
                online_time long
               );
                """)){

            tableCreate.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void addPlayTime(String uuid, long time){
        uuid = transformUUID(uuid);

        long total = time + getPlayTime(uuid);

        try(PreparedStatement update = connection.prepareStatement("""
                INSERT INTO logins
                VALUES (?, ?)
                ON DUPLICATE KEY UPDATE online_time=?
                """)){

            update.setString(1, uuid);
            update.setLong(2, total);
            update.setLong(3, total);

            update.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public long getPlayTime(String uuid){
        uuid = transformUUID(uuid);

        try(PreparedStatement statement = connection.prepareStatement("""
                    SELECT online_time FROM logins WHERE uuid=?
             """)){

            statement.setString(1, uuid);

            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                return resultSet.getLong(1);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return -1;
    }

    private String transformUUID(String uuid){
        return uuid.replaceAll("-", "").toLowerCase();
    }

    public static MySql getInstance() {
        return instance;
    }

    public Connection getConnection() {
        return connection;
    }
}
